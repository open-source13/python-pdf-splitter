# -*- coding: utf-8 -*-
import os
import sys
from PyPDF2 import PdfFileWriter, PdfFileMerger, PdfFileReader
from pathlib import Path


def splitdocument(filename):

    input_pdf = PdfFileReader(open(filename, "rb"))
    output_dir = os.path.dirname(filename)
    output_name = os.path.basename(filename)[:-4]

    for i in range(input_pdf.numPages):
        output_pdf_name = os.path.join(output_dir, "%s-Seite-%d.pdf" % (output_name, i + 1))
        output_pdf = PdfFileWriter()
        output_pdf.addPage(input_pdf.getPage(i))
        with open(output_pdf_name, "wb") as output_stream:
            output_pdf.write(output_stream)


def combine_documents(documents):

    if not documents:
        return

    first_file = documents[0]
    fpath = Path(first_file).absolute()
    out_path = f'{fpath}.combined.pdf'

    if os.path.isfile(out_path):
        return

    merger = PdfFileMerger()

    for pdf in documents:
        merger.append(pdf)

    merger.write(out_path)
    merger.close()


if __name__ == "__main__":

    mode = 'split'
    documents_to_combine = []

    for filename in sys.argv[1:]:
        if filename == '-c':
            mode = 'combine'

        if filename[-3:] == "pdf" and os.path.isfile(filename):
            if mode == 'split':
                splitdocument(filename=filename)
            if mode == 'combine':
                documents_to_combine.append(filename)

    if mode == 'combine' and documents_to_combine:
        combine_documents(documents_to_combine)
